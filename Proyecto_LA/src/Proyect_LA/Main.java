/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyect_LA;

import Proyect_LA.view.VPrincipal;
import javax.swing.UIManager;

/**
 *
 * @author edgla
 */
public class Main {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e) {
            // handle exception
        }
        VPrincipal vp = new VPrincipal();
        vp.setVisible(true);
    }
    
}
