/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyect_LA.control;

import Proyect_LA.view.VPrincipal;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 *
 * @author edgla
 */
public class Acciones {

    VPrincipal vp;
    //String contenido = "aux_1+45/==";
    int idx = 0;
    ArrayList<String> lista = new ArrayList();
    DefaultListModel modelo = new DefaultListModel();

    public Acciones(VPrincipal vp) {
        this.vp = vp;
    }

    public void automata() {
        String contenido = "";
        try {
            contenido = vp.getJtxtAreaContenido().getText();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,
                    "El area de texto no tiene contenido a evaluar",
                    "Advertencia", JOptionPane.WARNING_MESSAGE);
        }

        String lexema = "";

        char c = contenido.charAt(idx);

        elWhile:
        while (idx < contenido.length() && Character.isDefined(c)) {
            if (Character.isLetter(c)) {
                lexema = "";
                while (Character.isLetterOrDigit(c) || c == '_') {
                    lexema += c;
                    idx++;
                    if (isfin(idx, contenido.length())) {
                        System.out.println(idx);
                        lista.add(lexema);
                        continue elWhile;
                    }
                    c = contenido.charAt(idx);
                }
                System.out.println("[" + contenido.charAt(idx) + "]");

                lista.add(lexema);

            } else if (Character.isDigit(c) && c != 0) {
                lexema = "";
                while (Character.isDigit(c)) {
                    lexema += c;
                    idx++;
                    if (isfin(idx, contenido.length())) {
                        //System.out.println(idx);
                        lista.add(lexema);
                        continue elWhile;
                    }
                    c = contenido.charAt(idx);
                }
                lista.add(lexema);
            }
            if (Character.isLetterOrDigit(c)) {
                continue;
            }
            switch (c) {
                case '=':
                    char c2=c;
                    idx++;
                    if(c==c2){
                        lista.add("==");
                        idx++;
                        c2=' ';
                    }else{
                        lista.add("=");
                    }
                    c = contenido.charAt(idx);
                    break;
                case '+':
                    lista.add("+");
                    idx++;
                    if (isfin(idx, contenido.length())) {
                        continue;
                    }
                    c = contenido.charAt(idx);
                    //System.out.println('+');
                    break;
                case '-':
                    lista.add(c+"");
                    idx++;
                    if (isfin(idx, contenido.length())) {
                        continue;
                    }
                    c = contenido.charAt(idx);
                    break;
                case '*':
                    lista.add(c+"");
                    idx++;
                    if (isfin(idx, contenido.length())) {
                        continue;
                    }
                    c = contenido.charAt(idx);
                    break;
                case '/':
                    lista.add(c+"");
                    idx++;
                    if (isfin(idx, contenido.length())) {
                        System.out.println(idx);
                        continue;
                    }
                    c = contenido.charAt(idx);
                    break;
                case '(':
                    lista.add(c+"");
                    idx++;
                    if (isfin(idx, contenido.length())) {
                        continue;
                    }
                    c = contenido.charAt(idx);
                    break;
                case ')':
                    lista.add(c+"");
                    idx++;
                    if (isfin(idx, contenido.length())) {
                        continue;
                    }
                    c = contenido.charAt(idx);
                    break;
                case ',':
                    lista.add(c+"");
                    idx++;
                    if (isfin(idx, contenido.length())) {
                        continue;
                    }
                    c = contenido.charAt(idx);
                    break;
                case '.':
                    lista.add(c+"");
                    idx++;
                    if (isfin(idx, contenido.length())) {
                        continue;
                    }
                    c = contenido.charAt(idx);
                    break;
                case ';':
                    lista.add(c+"");
                    idx++;
                    if (isfin(idx, contenido.length())) {
                        continue;
                    }
                    c = contenido.charAt(idx);
                    break;
                case '<':
                    //lista.add(c);
                    idx++;
                    c = contenido.charAt(idx);
                    if(c=='='){
                        lista.add("<=");
                    }else if(c=='>'){
                        lista.add("<>");
                    }else{
                        lista.add("<");
                    }
                    if (isfin(idx, contenido.length())) {
                        continue;
                    }
                    idx++;
                    c = contenido.charAt(idx);
                    break;
                case '>':
                    idx++;
                    c = contenido.charAt(idx);
                    if(c=='='){
                        lista.add(">=");
                        
                    }else{
                        lista.add(">");
                    }
                    if (isfin(idx, contenido.length())) {
                        continue;
                    }
                    idx++;
                    c = contenido.charAt(idx);
                    break;
                default:
                    idx++;
                    if (isfin(idx, contenido.length())) {
                        continue;
                    }
                    c = contenido.charAt(idx);
                    //mostrarLista(lista);
                    break;
            }

        }
        //lista.add(lexema);
        mostrarLista(lista);
        //lista.clear();

    }

    private boolean isfin(int idx, int length) {
        return idx >= length;
    }

    public void mostrarLista(ArrayList lista) {
        vp.getJListElementos().setModel(modelo);
        for (int i = 0; i < lista.size(); i++) {
            System.out.println(lista.get(i));
            modelo.addElement(lista.get(i));
        }

    }

    public void limpiarTodo() {
        vp.getJtxtAreaContenido().setText("");
        modelo.clear();
        lista.clear();
        idx = 0;
        //vp.getJListElementos().
    }

    public void limpiarCajaTexto() {
       vp.getJtxtAreaContenido().setText(""); 
    }

    public void limpiarLista() {
        modelo.clear();
        lista.clear();
        idx = 0;
    }
    

}
