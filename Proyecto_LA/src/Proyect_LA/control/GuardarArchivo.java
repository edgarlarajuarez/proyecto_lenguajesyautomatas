/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyect_LA.control;

import Proyect_LA.view.VPrincipal;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author edgla
 */
public class GuardarArchivo {

    VPrincipal vp = new VPrincipal();

    public GuardarArchivo(VPrincipal vp) {
        this.vp = vp;
    }

    public void guardarArchivo() {
        try {
            String nombre = "";
            JFileChooser archivo = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("*.txt", "txt", "text");
            archivo.setFileFilter(filter);
            archivo.showSaveDialog(vp);
            File guarda = archivo.getSelectedFile();

            if (guarda != null) {
                try ( /*guardamos el archivo y le damos el formato directamente,
                 * si queremos que se guarde en formato doc lo definimos como .doc*/ 
                    FileWriter save = new FileWriter(guarda + ".txt")) {
                    save.write(vp.getJtxtAreaContenido().getText());
                }
                JOptionPane.showMessageDialog(null,
                        "El archivo se a guardado Exitosamente",
                        "Información", JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null,
                    "Su archivo no se ha guardado",
                    "Advertencia", JOptionPane.WARNING_MESSAGE);
        }
    }
}
