/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template archivo, choose Tools | Templates
 * and open the template in the editor.
 */
package Proyect_LA.control;

import Proyect_LA.view.VPrincipal;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Edgar Lara Juárez
 */
public class AbrirArchivo {

    VPrincipal vp;

    public AbrirArchivo(VPrincipal vp) {
        this.vp = vp;
    }

    public String abrirArchivo() throws UnsupportedLookAndFeelException {
        String aux = "", texto = "";
        try {
            /**
             * llamamos el metodo que permite cargar la ventana
             */
            JFileChooser archivo = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("*.txt", "txt", "text");
            archivo.setFileFilter(filter);
            archivo.showOpenDialog(vp);
           
            File abre = archivo.getSelectedFile();
             archivo.setCurrentDirectory(abre);
            /**
             * abrimos el archivo seleccionado
             */

            /**
             * recorremos el archivo, lo leemos para plasmarlo en el area de
             * texto
             */
            if (abre != null) {
                FileReader archivos = new FileReader(abre);
                BufferedReader lee = new BufferedReader(archivos);
                while ((aux = lee.readLine()) != null) {
                    texto += aux + "\n";
                }
                lee.close();
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ex + ""
                    + "\nNo se ha encontrado el archivo",
                    "ADVERTENCIA!!!", JOptionPane.WARNING_MESSAGE);
        }
        return texto;//El texto se almacena en el JTextArea
    }

}
